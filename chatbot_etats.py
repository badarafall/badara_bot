

import logging
import sys
import requests, time, math



from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

bot_token = sys.argv[1]

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

CHOIX, RESTAURANT, SORTIE, TOPMUSEES, TOPBARS, TOPCLUBS, LIEUX, COORDONNEES, DETAILS  = range(9)


def start(bot, update):
    reply_keyboard = [['Sortir', 'Restaurant']]

    update.message.reply_text(
        'Salut mon nom est Alexa. Je suis là pour te faire découvrir les meilleurs endroits de genève. \n\n'
        'Alors tu veut Sortir ou aller au Restaurant ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return CHOIX





def lesDiffSorties(bot, update):
    reply_keyboard_sortie = [
        ['Musées', 'Bars', 'Clubs']]

    update.message.reply_text('Je vois! T‘es un fetard. '
              'Dis moi qu‘elle type de sortie t‘interesse.',
              reply_markup = ReplyKeyboardMarkup(
                 reply_keyboard_sortie,
                 one_time_keyboard=True
             )
     )



    return SORTIE

def TopMusees(bot, update):
    reply_keyboard_topmusees = [
        ['top musees']]
    update.message.reply_text('Musée Barbier-Mueller \n'
                              'rue Jean-Calvin 10, 1204 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topmusees,
                                  one_time_keyboard=True
                              )
     )

    update.message.reply_text('Musée de Carouge \n'
                              'place de Sardaigne 2, 1227 Carouge GE \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topmusees,
                                  one_time_keyboard=True
                              )
                              )

    update.message.reply_text('Fondation Baur Musée des arts d‘Extrême-Orient \n'
                              'rue Munier-Romilly 8, 1206 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topmusees,
                                  one_time_keyboard=True
                              )
                              )

    return TOPMUSEES

def TopBars(bot, update):
    reply_keyboard_topbars = [
        ['top bars']]
    update.message.reply_text('l‘Eléphant dans la Canette \n'
                              'avenue du Mail 18, 1205 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topbars,
                                  one_time_keyboard=True
                              )
     )

    update.message.reply_text('The Mirror Bar \n'
                              'quai du Général-Guisan 34, 1204 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topbars,
                                  one_time_keyboard=True
                              )
                              )

    update.message.reply_text('Point Bar Club \n'
                              'rue du Marché 3Bis, 1204 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topbars,
                                  one_time_keyboard=True
                              )
                              )

    return TOPBARS

def TopClubs(bot, update):
    reply_keyboard_topclubs = [
        ['top clubs']]
    update.message.reply_text('Coupole Avenue Night Club \n'
                              'rue du Rhône 116, 1204 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topclubs,
                                  one_time_keyboard=True
                              )
     )

    update.message.reply_text('LE BAROQUE CLUB \n'
                              'place de la Fusterie 12, 1204 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topclubs,
                                  one_time_keyboard=True
                              )
                              )

    update.message.reply_text('Mazazique \n'
                              'rue Arnold Winkelried 6, 1201 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_topclubs,
                                  one_time_keyboard=True
                              )
                              )

    return TOPCLUBS




def LesDiffResto(bot, update):
    reply_keyboard = [['Italien', 'Asiatique', 'Indian', 'Francais', 'Africain']]

    user = update.message.from_user
    logger.info("Restaurants of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Je vois! T‘aime manger. '
                                  'Dis moi qu‘elle type de restaurant t‘interesse.',
                                  reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return RESTAURANT


def RestoItalien(bot, update):
        reply_keyboard_RestoItalien = [
            ['resto italien']]
        update.message.reply_text('La Matze \n'
                                  'rue Chaponnière 7, 1201 Genève \n',
                                  reply_markup=ReplyKeyboardMarkup(
                                      reply_keyboard_RestoItalien,
                                      one_time_keyboard=True
                                  )
                                  )

        return RESTAURANT


def RestoAsiatique(bot, update):
    reply_keyboard_RestoAsiatique = [
        ['resto asiatique']]
    update.message.reply_text('Restaurant BOKY \n'
                              'rue des Alpes 21, 1201 Genève \n',
                              reply_markup=ReplyKeyboardMarkup(
                                  reply_keyboard_RestoAsiatique,
                                  one_time_keyboard=True
                              )
                              )

    return RESTAURANT

def RestoIndian(bot, update):
        reply_keyboard_RestoIndian = [
            ['resto indian']]
        update.message.reply_text('An Indian Plaza \n'
                                  'rue de Monthoux 58, 1201 Genève \n',
                                  reply_markup=ReplyKeyboardMarkup(
                                      reply_keyboard_RestoIndian,
                                      one_time_keyboard=True
                                  )
                                  )

        return RESTAURANT

def RestoFrancais(bot, update):
        reply_keyboard_RestoFrancais = [
            ['resto francais']]
        update.message.reply_text('Entrecôte St-Jean \n'
                                  'boulevard Carl-Vogt 79, 1205 Genève \n',
                                  reply_markup=ReplyKeyboardMarkup(
                                      reply_keyboard_RestoFrancais,
                                      one_time_keyboard=True
                                  )
                                  )

        return RESTAURANT

def RestoAfricain(bot, update):
        reply_keyboard_RestoAfricain = [
            ['resto africain']]
        update.message.reply_text('Restaurant African Food \n'
                                  'rue de Zurich 32, 1201 Genève \n',
                                  reply_markup=ReplyKeyboardMarkup(
                                      reply_keyboard_RestoAfricain,
                                      one_time_keyboard=True
                                  )
                                  )

        return RESTAURANT

def appeler_opendata(path):
    url = "http://transport.opendata.ch/v1/" + path
    reponse = requests.get(url)
    return reponse.json()



def calcul_temps_depart(timestamp):
    seconds = timestamp-time.time()
    minutes = math.floor(seconds/60)
    if minutes < 1:
        return "FAUT COURIR!"
    if minutes > 60:
        return "> {} h.".format(math.floor(minutes/60))
    return "dans {} min.".format(minutes)


def afficher_arrets(update, arrets):
    texte_de_reponse = "Voici les arrets:\n"
    for station in arrets['stations']:
        if station['id'] is not None:
            texte_de_reponse += "\n/a" + station['id'] + " " + station['name']
    update.message.reply_text(texte_de_reponse)



def afficher_departs(update, departs):
    texte_de_reponse = "Voici les prochains departs:\n\n"
    for depart in departs['stationboard']:
        texte_de_reponse += "{} {} dest. {} - {}\n".format(
            depart['category'],
            depart['number'],
            depart['to'],
            calcul_temps_depart(depart['stop']['departureTimestamp'])
        )
    texte_de_reponse += "\nAfficher a nouveau: /a" + departs['station']['id']

    coordinate = departs['station']['coordinate']
    update.message.reply_location(coordinate['x'], coordinate['y'])
    update.message.reply_text(texte_de_reponse)


def bienvenue(bot, update):
    update.message.reply_text("Bonjour, je suis là pour te renseigner sur les horaires TPG. \n"
                              "Merci d'envoyer votre localisation (via piece jointe ou simplement en texte)"
                              )
    return LIEUX

def lieu_a_chercher(bot, update):
    resultats_opendata = appeler_opendata("locations?query=" + update.message.text)
    afficher_arrets(update, resultats_opendata)

    return COORDONNEES

def coordonnees_a_traiter(bot, update):
    location = update.message.location
    resultats_opendata = appeler_opendata("locations?x={}&y={}".format(location.latitude, location.longitude))
    afficher_arrets(update, resultats_opendata)

    return DETAILS

def details_arret(bot, update):
    arret_id = update.message.text[2:]
    afficher_departs(update, appeler_opendata("stationboard?id=" + arret_id))









    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(bot_token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            CHOIX: [
                RegexHandler('Restaurant', LesDiffResto),
                RegexHandler('Sortir', lesDiffSorties)
            ],

            SORTIE: [
                RegexHandler('Musées', TopMusees),
                RegexHandler('Bars', TopBars),
                RegexHandler('Clubs', TopBars),

            ],

            RESTAURANT: [
                RegexHandler('Italien', RestoAsiatique),
                RegexHandler('Asiatique', RestoAsiatique),
                RegexHandler('Indian', RestoIndian),
                RegexHandler('Francais', RestoFrancais),
                RegexHandler('Africain', RestoAfricain),


            ]

        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('transport', bienvenue)],

        states={
            LIEUX: [
                MessageHandler(Filters.text, lieu_a_chercher)
            ],

            COORDONNEES: [
                MessageHandler(Filters.location, coordonnees_a_traiter)

            ],

            DETAILS: [
                MessageHandler(Filters.command, details_arret)

            ]

        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()